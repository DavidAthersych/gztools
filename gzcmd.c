// gzcmd.c [170304]
// Command to apply gz compression/expansion to one file

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "zlib.h"

static void usage (int err, int argc, char **argv)
{
	int		i;
	if (err)	{
		fputs ("ERROR: Invalid command line\n", stderr);
		}
	fputs ("usage: gzcmd -compress|-expand srcfile dstfile\n", stderr);
	fprintf (stderr, "argc: %d\nProvided argument list:\n", argc);
	for (i=0; i<argc; i++)	{
		fprintf (stderr, "argv [%d]: %s\n", i, argv[i]);
		}
}//usage()
	

int main(int argc, char **argv)
// Simple command line interface to gz routines gzcmprss() and gzexpnd().
{
    int ret;

	fputs ("GZCMD - GZ compression/expansion utility\n", stderr);
	fputs ("   Developed 2017 Cynosure Computer Technologies Inc.\n", stderr);
	fputs ("   based on code ", stderr);
	fputs ("Copyright (C) 1995-2016 Jean-loup Gailly, Mark Adler\n", stderr);
	fputs ("Program may be used without charge.\n", stderr);

	if (argc != 4)	{
		usage (1, argc, argv);
		ret = -1;
	} else if (stricmp (argv[1], "-compress") == 0)	{
		fputs ("Perform compression:\n", stderr);
		fprintf (stderr, "\tInput: %s\n", argv[2]);
		fprintf (stderr, "\tOutput:%s\n", argv[3]);
		ret = gzcmprss (argv[2], argv[3]);
    } else if (stricmp (argv[1], "-expand") == 0)	{
		fputs ("Perform expansion:\n", stderr);
		fprintf (stderr, "\tInput: %s\n", argv[2]);
		fprintf (stderr, "\tOutput:%s\n", argv[3]);
		ret = gzexpnd (argv[2], argv[3]);
	} else	{
		usage (0, argc, argv);
		ret = 0;
		}
	if (ret < 0)	fprintf (stderr, "Error occurred: %d\n", ret);
	fputs ("Type return key to exit command", stderr);
	fflush (stderr);
	getchar ();
	return ret;
}//main()
